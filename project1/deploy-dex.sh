#!/usr/bin/env bash

helm upgrade --install \
  -n multi-idp project1-dex \
  dex/dex \
  --values=dex-config.yaml