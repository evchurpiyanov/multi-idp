#!/usr/bin/env bash

helm upgrade --install \
  -n multi-idp project1-oauth2 \
  k8s-at-home/oauth2-proxy \
  --values=oauth2-config.yaml