#!/usr/bin/env bash

helm upgrade --install \
  -n multi-idp shared-dex \
  dex/dex \
  --values=dex-config.yaml